# Kubernetes 101 - An introduction

I'm using [Marp](https://marp.app/) for the slides.

Run slides with:

```shell
make present
```

Generate PDF with:

```shell
make pdf
```

