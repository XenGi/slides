---
title: Kubernetes 101
description: An introduction
author: Ricardo Band
keywords: kubernetes,container,basics
---
<!-- theme: default -->
<!-- class: invert -->

![bg left:40% 80%](./k8s.svg)

# Kubernetes 101

## An introduction

---

# Stuff you should already know

- Containers
- Container engines
  - [docker][docker], [podman][podman] :heart:
- Bonus points for:
  - [docker-compose][docker-compose]
  - [podman-compose][podman-compose]

[docker]: https://www.docker.com/
[podman]: https://podman.io/
[docker-compose]: https://docs.docker.com/compose/
[podman-compose]: https://github.com/containers/podman-compose

<!--
- podman can run root less
- there is a docker-compose equivalent
- compose is basically running a kubernetes like deployment
-->

---

# Agenda part 1 - The basics

- 🧑 Workloads aka your app
- 🛋️ Environment
- 📦 Containers
- 🏡 Pods
- 🏭 Deployments
- 🏪 Services
- 💾 Volumes
- 🌆 Namespaces

<!--
If you want to explain it to your kid, you can stop after this.
The emojis probably make no sense, ignore them if they confuse you.
-->

---

# Agenda part 2 - Advanced topics

- 🏷️ Labels and 🔖 Taints
- 🏘️ ReplicaSets
- 🧰 DaemonSet
- 🗃️ StatefulSets
- 🌐 Ingress Controller
- 📑 Cluster DNS
- 💾 Container Storage Interface (CSI)
- 🕸️  Container Network Interface (CNI)
  - 👮 Network Policies

<!-- It's ok if you zone out somewhere around here. -->

---

![bg right 80%](./workload.drawio.svg)

# 🧑🛋️  Workloads and their environment

Example: Simple PHP app

- index.php
- PHP runtime ([php-fpm][php-fpm])
- Webserver ([NGINX][nginx])
- OS ([Alpine][alpine])

[php-fpm]: https://www.php.net/manual/en/install.fpm.php
[nginx]: https://www.nginx.com/
[alpine]: https://www.alpinelinux.org/

---

![bg right 80%](./container.drawio.svg)

# 📦 Containers

- Isolated environment
  - Linux namespace and cgroups
- Networking?
- Persistant storage?
- Scheduling, Load balancing

<!-- Networking: I smell another talk around here -->

---

![bg right 80%](./pod.drawio.svg)

# 🏡 Pods

- "Runnable unit of work"
- Holds Containers
- Connects to overlay network
- Container runtime interfaces (CRI)
  - containerd, CRI-O, Docker

---

![bg right 80%](./deployment.drawio.svg)

# 🏭 Deployment

- Manages:
  - Pods
  - Volumes
  - Secrets
- Types:
  - ReplicaSets
  - DaemonSets
  - StatefulSets

---

![bg right 100%](./service.drawio.svg)

# 🏪 Services

---

![bg right 100%](./volume.drawio.svg)

# 💾 Volumes

---

![bg right 100%](./namespace.drawio.svg)

# 🌆 Namepaces

- Isolates group of resources
- Names need to be unique

---

# You survived, take a cookie!

# :cookie:

---

# 💣 Hands on

Let's deploy a simple web app with a volume.

---

# 🏷️ Labels and 🔖 Taints

---

# 🏘️ ReplicaSets

- Maintain a stable set of replica Pods
- Pods are identical
- Good for scaling
- Finds Pods by their label 🏷️

---

# 🧰 DaemonSets

- Pods are started on every node of the cluster
- Useful for cluster wide services

---

# 🗃️ StatefulSets

- Fine control over storage
- Pods are started in order
- Pods have stable identities
  - redis-1, redis-2, redis-3, ...
- Useful for clustered services
  - E.g. primary pod needs to start first

---

# 🌐 Ingress Controller

- Load balancer
- Can be NGINX, HAProxy
- Different in Cloud kubernetes
- Handles HTTP(S) traffic to the cluster
- Kubernetes object: `Ingress`
- OpenShift object: `Route`

---

# 📑 Cluster DNS

- Pods: `<pod-ip-address>.<namespace-name>.pod.cluster.local`
- Services: `<service-name>.<namespace-name>.svc.cluster.local`
- Pods behind Services: `<pod-ip-address>.<service-name>.<namespace-name>.svc.cluster.local`
- /etc/resolv.conf:
  ```
  nameserver 2001:cafe:42:1::23
  search <namespace>.svc.cluster.local svc.cluster.local cluster.local
  options ndots:5
  ```

---

# 💾 Container Storage Interface (CSI)

---

# 🕸️  Container Netrwork Interface (CNI)

## 👮 Network Policies

---

# Kubernetes distribution

- 💻 On bare metal
  - [RedHat OpenShift][openshift]
  - [OKD][okd]
  - [Rancher][rancher]
  - [k3s][k3s]/[k3os][k3os] ([RaspberryPi][rpi])
- ⛅ In the cloud:
  - Amazon (EKS, ECS)
  - Google (GCP)

[openshift]: https://www.redhat.com/en/technologies/cloud-computing/openshift
[okd]: https://www.okd.io/
[rancher]: https://rancher.com/
[k3s]: https://k3s.io/
[k3os]: https://k3os.io/
[rpi]: https://www.raspberrypi.com/

---

# Still not enough?

- 🃏 Custom Resources
- 🔑 Secrets
- 📡 Control Plane
- 🔩 Operators
- 🕑 Jobs and CronJobs
- 🗺️ ConfigMaps
- 🔐 Pod Security Policies
- 🪪 Role Based Access Control (RBAC)
- 🪵 Resource Quotas

---

# Sources

- [Kubernetes Children’s Guide](https://www.cncf.io/phippy/the-childrens-illustrated-guide-to-kubernetes/)

---

![bg opacity blur](./avatar.png)

```

--

by Ricardo Band

mailto:email@ricardo.band
 https://www.ricardo.band/
```
