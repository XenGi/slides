How I ansible
=============

How to run the slides:

```sh
pipx install present
present slides.md
```

_[pipx](https://pypa.github.io/pipx/) installs python tools in a dedicated environment to prevent overlapping dependencies_

---

Made with ❤️ and 🐍.

