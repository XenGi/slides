---

---

# Mechanical Keyboards

## aka how to waste money fast 🕳️💵

---

# Agenda

- 🔍 Sizes
- 🔳 Keycaps
- 🎛️ Switches
- 🎚️ Stabilizers
- 🧠 PCBs
- 🧱 Cases
- ✨ Mods

---

# 🔍 Sizes

## Percentage of a full size keyboard

---

# 100% - Fullsize

![bg 100% left](./100percent_a.png)
![bg 100% right](./100percent_b.png)

---

# 75%

![bg 100% left](./75percent_a.png)
![bg 100% right](./75percent_b.png)

---

# 65%

---

# 40% - Planck

---

# Ergonomic

---

# 🔳 Keycaps

- uniform vs sculpted

## Cherry, OEM

- "the normal ones"

## SA, MDA

- tall, old school looking

## XDA, DSA

- one size fits all
- good for custom layouts

## Articans

- it's art

---

# 🎛️ Switches

## General facts

- Cherry “Mechanical X-Point” technology (MX) from early 1980s
- Gateron, Kailh, Razer, etc build compatible alternatives

**insert colorful picture of switches here**

---

# 🎛️ Switches

## Sizes

### Normal

- Cherry MX compatible
- Alps

### Low profile

- Cherry MX Low Profile
- Kailh Low Profile Choc v1
- Kailh Low Profile Choc v2

---

# 🎛️ Switches

## Types

### Tactile

- Bump when pressing down

### Linear

- No bump
- Speed variants with less pre-travel

### Clicky

- Tactile bump
- Audible click

---

# 🎛️ Switches

- Mounting: 5 Pin vs 3 Pin
- Materials:
  - Polycarbonate (PC) - usually top housing
  - Nylon (PA66) - usially bottom housing
  - Polyoxymethylene (POM) - usually stem
    - Self lubricating
- Actuation Force in grams
  - Bottom out
- Spring size in mm
- Travel distance in mm
  - Total and pre travel before actuation

---

# 🎚️ Stabilizers

---

# ✨ Mods

## Golden Toslink cables ahead! 😜

---

# ✨ Mods

## 💦 Lube

---

# ✨ Mods

## 🔉 Foam

---

# ✨ Mods

## Stickers

---

# ✨ Mods

## Films



